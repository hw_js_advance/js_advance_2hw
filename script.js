// Теоретичні питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// 1) jason.parse(str) характеный пример, если ошибка в данных то скрипит работаьт не будет
// 2) когда в объекте нет нужного свойства

// Завдання
// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж и i-p Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Myp",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Teppi Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

function createTree(data, target = document.body) {
  for (const i of data) {
    const ulElem = document.createElement("ul");
    target.append(ulElem);
    for (const x in i) {
      const liElem = document.createElement("li");
      ulElem.append(liElem);
      liElem.innerText = i[x];
    }
  }
}

const newBooks = books.filter(function ({ author, name, price }) {
  try {
    let result = "a new error";

    if (author === undefined) {
      result = result + ", No author";
    }
    if (name === undefined) {
      result = result + ", No name";
    }
    if (price === undefined) {
      result = result + ", No price";
    }

    if (result !== "a new error") {
      throw new Error(result);
    }
    return { author, name, price };
  } catch (e) {
    console.log(e.message);
  }
});

createTree(newBooks);
